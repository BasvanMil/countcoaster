import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Achtbaan toevoegen',
      description: 'Hiermee kan een gebruiker een nieuwe achtbaan toevoegen aan het lijstje.',
      scenario: [
        'Gebruiker zoekt de achtbaan op',
        'De gebruiker geeft een rating aan de achtbaan',
        'De gebruiker heeft de mogelijkheid om een review achter te laten (nieuwe pagina, daarna weer terug naar pagina van achtbaan)',
        'Achtbaan is terug te vinden op het profiel van de gebruiker en gebruiker ziet dat de rating gegeven is'
      ],
      actor: 'Gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Achtbaan is toegevoegd en rating is gegeven.'
    },
    {
      id: 'UC-02A',
      name: 'Achtbaan verwijderen',
      description: 'Hiermee kan een gebruiker een achtbaan verwijderen van het lijstje.',
      scenario: [
        'Gebruiker gaat naar zijn lijstje',
        'De gebruiker zoekt achtbaan met zoekoptie of zoekt met de hand',
        'Gebruiker klikt op prullenbakje',
        'Achtbaan is verwijderd en verdwijnt uit de lijst, gebruiker redirect niet naar andere pagina '
      ],
      actor: 'Gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Achtbaan is verwijderd uit de lijst van de gebruiker.'
    },
    {
      id: 'UC-02B',
      name: 'Top lijst maken',
      description: 'Hiermee kan een gebruiker een selectie van achtbanen op een rijtje zetten',
      scenario: [
        'Gebruiker gaat via zijn profiel naar zijn tops',
        'Gebruiker kan nieuwe top maken (bijvoorbeeld top 10 wooden coasters)',
        'Gebruiker zoekt de achtbanen op en voegt ze toe aan de lijst',
        'Gebruiker kan de banen op volgorde zetten van beste naar minder goed',
        'Gebruiker slaat lijst op en krijgt overzicht te zien'
      ],
      actor: 'Gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Top list is gemaakt'
    },
    {
      id: 'UC-02C',
      name: 'Top lijst aanpassen',
      description: 'Gebruiker',
      scenario: [
        'Gebruiker gaat via zijn profiel naar zijn tops',
        'Gebruiker kan een top kiezen om aan te passen',
        'Gebruiker kan achtbanen toevoegen/verwijderen/plek aanpassen',
        'Gebruiker slaat lijst op en krijgt overzicht te zien'
      ],
      actor: 'Gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Achtbaan is verwijderd uit de lijst van de gebruiker.'
    },
    {
      id: 'UC-03',
      name: 'Achtbaan toevoegen aan database',
      description: 'Hiermee kan een administrator een achtbaan toevoegen',
      scenario: [
        'Een nieuwe achtbaan is open en een administrator moet deze toevoegen',
        'De administrator kan via het adminpanel een achtbaan toevoegen aan de database',
        'Administrator vult gegevens van achtbaan in'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition:
        'Achtbaan toegevoegd aan de database en gebruikers kunnen hem nu toevoegen aan hun lijstje.'
    },
    {
      id: 'UC-03A',
      name: 'Achtbaan verwijderen van database',
      description: 'Hiermee kan een administrator een achtbaan verwijderen',
      scenario: [
        'Een achtbaan is verkeerd ingevoerd/bestaat niet en moet verwijderd worden',
        'De administrator kan via het adminpanel een achtbaan verwijderen uit de database',
        'Administrator drukt op prullenbakje bij de achtbaan'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition:
        'Achtbaan toegevoegd aan de database en gebruikers kunnen hem nu toevoegen aan hun lijstje.'
    },
    {
      id: 'UC-03B',
      name: 'Achtbaan aanpassen in database',
      description: 'Hiermee kan een administrator een achtbaan aanpassen',
      scenario: [
        'Informatie van de achtbaan is onjuist en moet aangepast worden',
        'De administrator kan via het adminpanel een achtbaan aanpassen',
        'Administrator drukt op potlood bij de achtbaan om aan te passen',
        'Wanneer juiste gegevens zijn ingevuld kan die het opslaan'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition:
        'Achtbaan toegevoegd aan de database en gebruikers kunnen hem nu toevoegen aan hun lijstje.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
